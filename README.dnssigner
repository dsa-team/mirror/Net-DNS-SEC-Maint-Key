Net::DNS::SEC::Maint::Key and Net::DNS::SEC::Maint::Zone package.
    Standalone dnssigner with keymaintenance database.

  Warning
    This is very condenced, please refer to the documentation in the doc
    subdirectory.

PURPOSE
    The this program suite is designed to ease DNSSEC key management. The
    suite contains, besides a number of libraries, the following programs:

       maintkeydb    - A shell in which you maintain your keys
       dnssecmaint-config
                     - A tool to create an initial config.
       dnssec-copyprivate 
                     - Copies keypairs out of the keydatabase to a different location
                       (Useful in combination with a dynamic zone.)

       dnssigner     - A signer that uses the keydatabase to sign zones.

    The first three programs come with Net::DNS::SEC::Maint::Key, dnssigner
    is distributed as part of Net::DNS::SEC::Maint::Key.

INSTALL
    In short: - Install packages - Create group dnssecmt - Run
    dnssecmaint-config - Create log and database directory with proper file
    permissions.

    To install the the program suite you have to install
    Net::DNS::SEC::Maint::Key and Net::DNS::SEC::Maint::Zone (in that
    order).

    unpack the distributions and in the distribution do:

      perl Makefile.PL

      make 
      make install

    If, after you typed perl Makefile.PL, you get messages like "Warning:
    prerequisite XML::LibXML failed to load you have to install these
    prerequisites using CPAN first. There are a lot of prerequisites. You
    may try to install them through the use of the
    Bundle-Private-KeystoreSignerPre bundle that can be found at the same
    location as you downloaded the two main distribution.

    To run the program in an environment where multiple users need to access
    the system you have to create a group called "dnssecmt" in /etc/group.
    (The name of the group can be specified in the configuration file as
    well).

    The program uses a number of defaults that can be overwritten by a
    configuration file or a set of environment variables. Use the program
    'dnssecmaint-config' to write a configuration file. By default the
    configuration file resides in /usr/local/etc/dnssecmaint.conf. You may
    use the DNSSECMAINT_CONFFILE environment variable to point to another
    path.

    Per default the following directories are configured to be used and you
    will need to create them if you have not specified other directories in
    your config file (using the 'dnssecmaint-config' script) -
    /usr/local/var/dnssec_maint/DNS_KEY_DB is where the key database resides
    - /usr/local/var/dnssec_maint/tmp is used for storage of tmp files

    Make sure these directories exists and that the group permissions are
    set correctly.

  Example setup session.
    After installing Net::DNS::SEC and Net::DNS::SEC::Zone and after adding
    dnssecmt to /etc/group perform the following as root:

        mkdir /usr/local/var/dnssec_maint/           
        mkdir /usr/local/var/dnssec_maint/DNS_KEY_DB 
        mkdir /usr/local/var/dnssec_maint/log
        mkdir /usr/local/var/dnssec_maint/tmp
        chmod -R o-rwx /usr/local/var/dnssec_maint 
        chgrp -R  dnssecmt  /usr/local/var/dnssec_maint 
        chmod -R g+rwX /usr/local/var/dnssec_maint
        dnssecmain-config
       <... input/output skipped > 

USAGE
    Start "maintkeydb -i" and type 'help' to see all the commands. Type
    'create' to create your first key.

    After you created key using the maintain shell you can sign your zone by
    calling dnssigner <zone.file>

    type 'man dnssigner.pl' for more information on the flags.

    Note: automated rollovers will only work under specific circumstances
    perldoc Net::DNS::SEC::Key.pm for details

  Example session
    As a user who is a member of the 'dnssecmt' group fire up "maintkeydb"

       olaf> maintkeydb -i
       Command? >create
       Force creation?
       [skip|force] > skip
       Create a KSK, a ZSK or both?
       type > both
       Algorithm?
       algorithm > RSASHA1
       Key Size?
       keysize > default
       Enter one or more zone names
       zone(s) > example.com
       Using default RSA ZSK Size (768)
       Using default RSA ZSK Size (2048)
        Created 3 keys for example.com
       Command? >
       Command? >
       Command? >list
       active, inactive, rollover or published keys?
       state > allstates
       KSK or ZSK?
       type > both
       Enter one or more zone names
       zone(s) > all
       example.com        0768 RSASHA1 11801   ZSK published  (0d00h05m)
       example.com        0768 RSASHA1 16961   ZSK active     (0d00h05m)
       example.com        2048 RSASHA1 61725   KSK active     (0d00h05m)
       Command? >exit
   
       olaf> dnssigner -o example.com. example.com.signed
       Output written to :example.com.signed 
       olaf> 

  Example script for Dynamic zones
    A script that uses the various commands to keep signatures in dynamic
    zones up to date can be found in the example directory of directory of
    Net::DNS::SEC::Zone.

COPYRIGHT
    Copyright Notice and Disclaimer

    Copyright (c) 2002 - 2005 RIPE NCC. Author Olaf M. Kolkman
    <net-dns-sec@ripe.net>

    All Rights Reserved

    Permission to use, copy, modify, and distribute this software and its
    documentation for any purpose and without fee is hereby granted,
    provided that the above copyright notice appear in all copies and that
    both that copyright notice and this permission notice appear in
    supporting documentation, and that the name of the author not be used in
    advertising or publicity pertaining to distribution of the software
    without specific, written prior permission.

    THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
    INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS; IN NO
    EVENT SHALL AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
    DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
    PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
    ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
    THIS SOFTWARE.

  Version information
    Generated from: $Id: README.dnssigner.pod,v 1.11 2005/06/27 14:39:50
    olaf Exp $

