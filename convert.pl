#!/usr/local/bin/perl -w -Iblib/lib
=head1 convert_db

Small tool to convert your existing DB from the old to the new format.

Make sure DNSSECMAINT_CONFFILE is set to where your configuration 
file is located.


=cut

use strict;

my $confpath="t/dnssecmaint.conf";
$ENV{"DNSSECMAINT_CONFFILE"}=$confpath;



use Net::DNS::SEC::Maint::Key;


my $keydb=Net::DNS::SEC::Maint::Key->new();
my @keyobjects=$keydb->get_all();
my $k;
foreach  $k ( @keyobjects ){
  print "----------------------------\n".
    "Working on:\n";
  print $k->keyrrstring."\n";
  my $adminfile=$k->get_keypath;
  $adminfile =~ s/private$/adm/;
  # 'Determine if this is a zone or keysigning key... we 
  # will later use a bit for this...
  my $zoneorkey="---";
  open(ADM,"< $adminfile") || die "Could not open $adminfile";
  while ( <ADM> ){
    chop;
    $zoneorkey="zonesigning" if /^USAGE: zonesigning/;
    $zoneorkey="keysigning" if /^USAGE: keysigning/;
  }
  close ADM;
  print "Puprose set to :$zoneorkey \n";
  $k->{"_key_attributes"}->{"purpose"}=$zoneorkey;
  $adminfile =~ s/adm$/active/;
  $k->{"_key_attributes"}->{"state"}="ACTIVE" if ( -f $adminfile );
  $adminfile =~ s/active$/published/;
  $k->{"_key_attributes"}->{"state"}="PUBLISHED" if ( -f $adminfile );
  $k->{"_key_attributes"}->{"state"}="INACTIVE" if !  $k->{"_key_attributes"}->{"state"};
  print "State set to :".$k->{"_key_attributes"}->{"state"}."\n";
  $k->_write_attributes;
  

}
