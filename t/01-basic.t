# -*-perl-*-
# Test script for  Net::DNS::SEC::Maint::Maint
# $Id: 01-basic.t,v 0.8 2003/05/08 15:32:01 olaf Exp $
# 
# Called in a fashion simmilar to:
# /usr/bin/perl -Iblib/arch -Iblib/lib -I/usr/lib/perl5/5.6.1/i386-freebsd \
# -I/usr/lib/perl5/5.6.1 -e 'use Test::Harness qw(&runtests $verbose); \
# $verbose=0; runtests @ARGV;' t/01-basic.t

use Test::More tests=>1;
use strict;

#
# This file intentionally does not contain any tests.
# The file is considdered a CVS Zombie.
#

ok (1,"Empty Test");
