# -*-perl-*-
# Test script for  Net::DNS::SEC::Maint::Maint
# $Id: 00-config.t,v 1.4 2004/05/19 13:28:05 olaf Exp $
# 
# Called in a fashion simmilar to:
# /usr/bin/perl -Iblib/arch -Iblib/lib -I/usr/lib/perl5/5.6.1/i386-freebsd \
# -I/usr/lib/perl5/5.6.1 -e 'use Test::Harness qw(&runtests $verbose); \
# $verbose=0; runtests @ARGV;' t/01-basic.t

use Test::More tests=>8;
use strict;

########################################################
# Log 4 perl initialization
use Log::Log4perl;
my $log4perlconf="t/log4perl.conf";
Log::Log4perl::Config->allow_code(0);
if (-f $log4perlconf ){
    diag("Using log4perl config from  $log4perlconf ");
    Log::Log4perl->init($log4perlconf)
    }
else {
    my $conf = q(
 log4perl.rootLogger                = DEBUG, LOGFILE
 log4perl.appender.LOGFILE          = Log::Log4perl::Appender::File
 log4perl.appender.LOGFILE.filename = test.log
 log4perl.appender.LOGFILE.layout   = Log::Log4perl::Layout::PatternLayout
 log4perl.appender.LOGFILE.layout.ConversionPattern = %d %p> %F{1}:%L %M - %m%n
 );
    Log::Log4perl->init(\$conf);
}

####################################


# The configgile path is read from the variable below.
my $confpath="t/dnssecmaint.conf";
$ENV{"DNSSECMAINT_CONFFILE"}=$confpath;


BEGIN {use_ok('Net::DNS::SEC::Maint::Key::Config');                                  # test 1
     };


my $keyconf=Net::DNS::SEC::Maint::Key::Config->new;
ok($keyconf,                "new() returned something");                 # test 2


$keyconf->set_default_conf;

is ($keyconf->getconf("dsakeysizezone"),512, "getconf read dsasizezone");    # test 4


$keyconf->read_config_file;

# Assess if the confile has been read...
is ($keyconf->getconf("testconfig"),"testvalue", "getconf read appropriate confille");    # test 3

# Try one of rhe vallues
is ($keyconf->getconf("dsakeysizezone"),768, "getconf read dsasizezone");    # test 4



my $testconf="Something something";
my $testdescr="Something else";
$keyconf->setconf ("dnssec_keygen",$testconf,$testdescr);


is ($keyconf->getconf ("dnssec_keygen"),$testconf,"setconf-getconf consistent");  # test 6


is ($keyconf->getdescription ("dnssec_keygen"),$testdescr,
      "setconf-getdescription consistent");                           # test 7

is ($keyconf->getconf ("conffile"),$confpath,"Conf file is OK");  # test 8

####
#  VARIOUS CONFIG TESTS
#

diag ("tests are finished");
