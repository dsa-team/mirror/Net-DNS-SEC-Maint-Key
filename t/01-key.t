# -*-perl-*-
# Test script for  Net::DNS::SEC::Maint::Key
# $Id: 01-key.t,v 1.16 2005/06/24 14:44:01 olaf Exp $
# 
# Called in a fashion simmilar to:
# /usr/bin/perl -Iblib/arch -Iblib/lib -I/usr/lib/perl5/5.6.1/i386-freebsd \
# -I/usr/lib/perl5/5.6.1 -e 'use Test::Harness qw(&runtests $verbose); \
# $verbose=0; runtests @ARGV;' t/01-basic.t


#
#  These tests depend on the database as stored in t/keydb at the moment of
#  unpacking the tar ball. 
#
#  If the tests do not finish at some point there may be garbage left in the
#  Database that corrupt future test.
#  rm -Rf t/keydb/bla.foo  may be sufficient to clean up mess left behind.
#  


use Test::More;
use Data::Dumper;
use File::Basename;
use strict;


########################################################
# Log 4 perl initialization
use Log::Log4perl;
my $log4perlconf="t/log4perl.conf";

Log::Log4perl::Config->allow_code(0);
if (-f $log4perlconf ){
    Log::Log4perl->init($log4perlconf)
    }
else {
    my $conf = q(
 log4perl.rootLogger                = DEBUG, LOGFILE
 log4perl.appender.LOGFILE          = Log::Log4perl::Appender::File
 log4perl.appender.LOGFILE.filename = test.log
 log4perl.appender.LOGFILE.layout   = Log::Log4perl::Layout::PatternLayout
 log4perl.appender.LOGFILE.layout.ConversionPattern = %d %p> %F{1}:%L %M - %m%n
 );
    Log::Log4perl->init(\$conf);
}

####################################



# Clean up berfore starting.... If there are any keys left from a previous run
# we will have failures
unlink <t/keydb/bla.foo/*>;
rmdir "t/keydb/bla.foo";


my $skip_creation=0;     # Set to 1 if you want to skip (the somewhat timely) 
                         # creation tests

my $confpath="t/dnssecmaint.conf";
$ENV{"DNSSECMAINT_CONFFILE"}=$confpath;

$ENV{"PATH"}=$ENV{"PATH"}.":/usr/local/sbin/:";


my $gid=getgrnam "dnssecmt";
if (!defined $gid){
  print "    dnssecmt is an unknown group\n";
  print "    Please add the group to /etc/group or change the maintgroup \n";
  print "    I will try to use your default group.\n";
  $ENV{"DNSSECMAINT_MAINTGROUP"}=getgrgid($();
}



diag ("The tests depend on dnssec-keygen and the openssl command to be in your path");


my $nokeygen=0;
my $noopenssl=0;

my $dnssec_keygen_path;
my $openssl_path;
use Shell qw (which);
$dnssec_keygen_path = which("dnssec-keygen");
$dnssec_keygen_path =~ s/\s+$//;

$openssl_path = which("openssl");
$openssl_path =~ s/\s+$//;

if (!$dnssec_keygen_path ){
    diag "dnssec-keygen not found\n";
    diag "Change your path to include dnssec_keygen\n";
    $nokeygen=1;
}


if (!$openssl_path ){
    diag "openssl not found\n";
    diag "Change your path to include openssl\n";
    $nokeygen=1;
}

# I wonder how portable this is...
if ($dnssec_keygen_path =~ /no *. in/ ){
  diag $dnssec_keygen_path ."\n";
  diag "Change your path to include dnssec_keygen\n";
  $nokeygen=1;
}
#extra test
if ( !( -x $dnssec_keygen_path )){
  diag $dnssec_keygen_path." is not executable\n";
  diag "Change your path to include dnssec_keygen\n";
  $nokeygen=1;
}


# I wonder how portable this is...
if ($openssl_path =~ /no *. in/ ){
  diag $openssl_path ."\n";
  diag "Change your path to include openssl\n";
  $nokeygen=1;
}
#extra test
if ( !( -x $openssl_path )){
  diag $openssl_path." is not executable\n";
  diag "Change your path to include openssl\n";
  $nokeygen=1;
}

$ENV{"DNSSECMAINT_DNSSEC_KEYGEN"}=$openssl_path. " rand -out t/random 100000 ;".$dnssec_keygen_path. " -r t/random ";



if ($nokeygen){
    plan skip_all =>  "critical programs not found";
    exit;
}else{
    plan tests=>70;

    ok(1,"Ready to test");  # test 1  (otherwise the numbering is messed up).

}





BEGIN {use_ok('Net::DNS::SEC::Maint::Key');                                 
     };





if (! ok ($Net::DNS::RR::RR{"KEY"},"KEY RR is available through Net::DNS::RR")){
    diag ("This test is an indication that the Net::DNS::SEC failed to load properly");
    diag ("You may want to test if Net::DNS::SEC and all its dependencies are installed");
    die "Giving up all further tests: Check the diagnostics above.";

}
                                                                     # test 2



my $key=Net::DNS::SEC::Maint::Key->new;
ok($key,                "new() returned something");                 # test 3

is (ref($key),"Net::DNS::SEC::Maint::Key","Net::DNS::SEC::Key object creation");           # test 4

# Assess if the confile has been read...
# A Key::Config object is embeded within the key object.
is ($key->getconf("testconfig"),"testvalue", "getconf read appropriate confille");    # test 5


# Try one of rhe vallues
is ($key->getconf("dsakeysizezone"),768, "getconf read dsasizezone");    # test 6


                                                                # test 7-11
ok ($key->valid_algorithm("RSA"), "RSA is valid");
ok ($key->valid_algorithm("DSA"), "DSA is valid");
ok ($key->valid_algorithm("RSAMD5"), "RSAMD5 is valid");
ok ($key->valid_algorithm("RSASHA1"), "RSASHA1 is valid");
ok (! $key->valid_algorithm("DUMMY"), "DUMMY is not valid");

SKIP: {
  skip "Running test from CVS environment", 1 if  -d "t/keydb/CVS" ;
  is(join (" ",$key->get_available_domains), "dacht.net","get_available_domains");
                                                                #test 12
}



SKIP: {
  skip "Running test from distribution", 1 if  ! -d "t/keydb/CVS" ;
  is(join (" ",$key->get_available_domains), "CVS dacht.net","get_available_domains");
 								 #test 13
}

SKIP: {
  skip $skip_creation?"Skipping creation tests":"No dnssec-keygen found on the system", 3 if $nokeygen || $skip_creation;

  #  Create a key 
     $key->create("bla.foo","RSASHA1",768,"zonesigning",1);
     ok ( ($key->get_active("bla.foo"))[0]->get_keypath =~ /^t\/keydb\/bla\.foo\/Kbla\.foo\.\+005\+(\d+)\.private$/, 
	  "active zone keys for test zone");
                                                                # test 14
     is ((($key->get_active("bla.foo"))[0]->keysize),768,"Keysize method returns correct value");
                                                                # test 15
# Clean the directory with the bla.foo keys.
 };  #  end SKIP



 SKIP: {
     skip $skip_creation?"Skipping creation tests":"Running test from CVS environment", 1 if  -d "t/keydb/CVS" || $skip_creation;
     is(join (" ",$key->get_available_domains), "bla.foo dacht.net","get_available_domains");
     			                                        #test 16
 }


 SKIP: {
     skip $skip_creation?"Skipping creation tests":"Running test from distribution", 1 if  ! -d "t/keydb/CVS"  || $skip_creation;
     is(join (" ",$key->get_available_domains), "CVS bla.foo dacht.net","get_available_domains");
     			                                        #test 17
 }
#  We know the exact content of the dacht.net keydb in the test setus
#

my $testarray="003+39372 005+12449 005+21827 005+28094 005+37841 005+39671 005+44699 005+56654";

my $algoidarray= join " ", $key->get_algo_id_pairs("dacht.net");

is($algoidarray, $testarray, "get_algo_id_pairs returned the complete set of pairs");
     			                                        #test 18


is($key->fetch("dacht.net","RSASHA1","21827"),0,"key fetched succesful");
     			                                        #test 19




is ($key->get_keypath,"t/keydb/dacht.net/Kdacht.net.+005+21827.private","get_keypath returns the proper path");
     			                                        #test 20


ok ($key->is_ksk,"Key is key signing key");
     			                                        #test 21
ok (! $key->is_active,"Key is not active");
     			                                        #test 22

is ($key->set_active,1,"set state worked");

     			                                        #test 23
ok ( $key->is_active,"Key is not active");
     			                                        #test 24
is ($key->set_inactive,1,"Toggling worked");
     			                                        #test 25
ok (! $key->is_active,"Key is not active");
     			                                        #test 26
is($key->fetch("t/keydb/dacht.net/Kdacht.net.+005+37841.private"),0,"fetch succesful");
     			                                        #test 27

ok ($key->is_zsk,"Key is zone signing key");
     			                                        #test 28

ok (! $key->is_ksk,"Key is not a key signing key");
     			                                        #test 29
is ($key->get_keypath,"t/keydb/dacht.net/Kdacht.net.+005+37841.private",
"get_keypath returns the proper path");
     			                                        #test 30

is($key->fetch("t/keydb/bla.foo/Kbla.foo.+005+12345.private"),"t/keydb/bla.foo/Kbla.foo.+005+12345.private not found","fetch proper error response");
     			                                        #test 31



is($key->fetch("dacht.net","RSASHA1","21828"),"algorithm-keyid not found for dacht.net RSASHA1 21828","fetch proper error response");
     			                                        #test 32






 SKIP: {
  my $key2=Net::DNS::SEC::Maint::Key->new();
  my @keyarray2;
  skip "Key not created",3 if $skip_creation;
  is (@keyarray2=$key2->get_all("bla.foo"),1,"One key for dacht.net returned");
     			                                        #test 33
  my $keypath=$keyarray2[0]->get_keypath;
  $keyarray2[0]->deletekey;
  is ($keyarray2[0]->get_keypath,"","Delete blanked keypath");
     			                                        #test 34

  my $newkeypath= dirname($keypath)."/Expired_Keys/".basename($keypath);
  ok ( -f  $newkeypath, "Key moved ot Expired_Keys subdir");
     			                                        #test 35
 }



my @keyobjects=$key->get_all;



my $k;
my @t;
foreach  $k  (@keyobjects){
  push @t, $k->get_keypath;
}


my $teststring="t/keydb/dacht.net/Kdacht.net.+003+39372.private,t/keydb/dacht.net/Kdacht.net.+005+12449.private,t/keydb/dacht.net/Kdacht.net.+005+21827.private,t/keydb/dacht.net/Kdacht.net.+005+28094.private,t/keydb/dacht.net/Kdacht.net.+005+37841.private,t/keydb/dacht.net/Kdacht.net.+005+39671.private,t/keydb/dacht.net/Kdacht.net.+005+44699.private,t/keydb/dacht.net/Kdacht.net.+005+56654.private";
is (join(",",sort @t),$teststring,"getall keys");
     			                                        #test 36




my @activekeys;



is (@activekeys=$key->get_active("dacht.net","DSA"),1,"There is only one active DSA key");
     			                                        #test 37
is ( $activekeys[0]->get_keypath,"t/keydb/dacht.net/Kdacht.net.+003+39372.private","Proper path to DSA key");
     			                                        #test 38







# More state tests
$key->fetch("dacht.net","RSASHA1","21827"),0,"key fetched succesful";

is ($key->get_keypath,"t/keydb/dacht.net/Kdacht.net.+005+21827.private","get_keypath returns the proper path");
     			                                        #test 39


ok ($key->is_ksk,"Key is key signing key");
     			                                        #test 40
ok ($key->is_inactive,"Key is inactive");
     			                                        #test 41

is ($key->set_active,1,"Toggling worked");
     			                                        #test 42

ok ( $key->is_active,"Key is active");
     			                                        #test 43

ok (! $key->is_inactive,"Key is active, thus not inactive");
     			                                        #test 44

ok (! $key->is_published,"Key is active, thus not published");

     			                                        #test 45

is ($key->set_inactive,1,"Set inactive");
     			                                        #test 46

ok (! $key->is_active,"Key is  active");
     			                                        #test 47





is ($key->set_published,1,"Toggling worked");

     			                                        #test 48

ok ( $key->is_published,"Key is  published");
     			                                        #test 49

ok (! $key->is_active,"Key is  published, thus not active");
     			                                        #test 50

ok (! $key->is_inactive,"Key is  published, thus not published");
     			                                        #test 51

is ($key->set_inactive,1,"Set inactive");
     			                                        #test 52

ok ($key->is_inactive,"Key is not active");
     			                                        #test 53

my $keyset1="dacht.net.	3600	IN	KEY	256  3  3 ( 
			ANkBgSfpnbjsgbjKibnBAB3OYb2r7EblxCKF
			mxkXqzWbWkDt6bVJyC/+iabEbBQkAoyZJBsy
			s/PIm11yXIfPVGPYFCfO4gB/IrQ+C83A2RyY
			L2wwxVjrUkv1jhlTZoCyxzuZGINvm+NkPMlB
			gitunOJ07cregISz3jTPrMg52j6h9Ps+Yfla
			vRfc1gvWHC/9niIcXxEIPeEKcwsGQz2jCH/E
			irMJ9BkhhGj8Ku/nmZtuzc6v+O9OVXizXm1O
			WKiBYdEjKl0aw7hHsKGD7cVzriKDL0IL 
			) ; Key ID = 39372
dacht.net.	3600	IN	KEY	256  3  5 ( 
			AQMDhBwhRyUtFdahOZYNxbcIyFrYTyP0MU5q
			asxsPReMqfgw981GSukcoTgvyoAQ/Xi7j9aw
			PTahijJ/OTZA7HDyVek6UxexCvHVmYuCGpC1
			Jt5iYbM33tbNGneRNHwLZNXpficn 
			) ; Key ID = 12449
dacht.net.	3600	IN	KEY	256  3  5 ( 
			AQMDFZSIE6N+UIXDJiVKtIEMB6lZPb8i29IW
			m3WG2wQ/SA1qgkwGKZbU7/yy603NTy8emZu/
			WP7P5UoMfZqxgsNF+uDjxjej7S2BINN346+C
			7PJNOKYC+X7v43zf3ir1P5CqbYJL 
			) ; Key ID = 39671
dacht.net.	3600	IN	KEY	256  3  5 ( 
			AQPDvaQlOBmS/l1T415xdfnIpgpm06aKslQq
			9oMJnLbDzB3H8ByJbKWdTMrztzdru+mO2ZT+
			y13/cRh6tRzoO2eqZzhZECzTP4FoSGiJuYVh
			EabYi/hiNLzUYQ1Rgj2Tf3k= 
			) ; Key ID = 44699
";


is( $key->get_keyset("dacht.net"), $keyset1, "getkeyset returns expected keys as well");
     			                                        #test 54
is ($key->set_published,1,"Non trivial toggling worked again");
     			                                        #test 55

my $keyset2="dacht.net.	3600	IN	KEY	256  3  3 ( 
			ANkBgSfpnbjsgbjKibnBAB3OYb2r7EblxCKF
			mxkXqzWbWkDt6bVJyC/+iabEbBQkAoyZJBsy
			s/PIm11yXIfPVGPYFCfO4gB/IrQ+C83A2RyY
			L2wwxVjrUkv1jhlTZoCyxzuZGINvm+NkPMlB
			gitunOJ07cregISz3jTPrMg52j6h9Ps+Yfla
			vRfc1gvWHC/9niIcXxEIPeEKcwsGQz2jCH/E
			irMJ9BkhhGj8Ku/nmZtuzc6v+O9OVXizXm1O
			WKiBYdEjKl0aw7hHsKGD7cVzriKDL0IL 
			) ; Key ID = 39372
dacht.net.	3600	IN	KEY	256  3  5 ( 
			AQMDhBwhRyUtFdahOZYNxbcIyFrYTyP0MU5q
			asxsPReMqfgw981GSukcoTgvyoAQ/Xi7j9aw
			PTahijJ/OTZA7HDyVek6UxexCvHVmYuCGpC1
			Jt5iYbM33tbNGneRNHwLZNXpficn 
			) ; Key ID = 12449
dacht.net.	3600	IN	KEY	256  3  5 ( 
			AQMDFZSIE6N+UIXDJiVKtIEMB6lZPb8i29IW
			m3WG2wQ/SA1qgkwGKZbU7/yy603NTy8emZu/
			WP7P5UoMfZqxgsNF+uDjxjej7S2BINN346+C
			7PJNOKYC+X7v43zf3ir1P5CqbYJL 
			) ; Key ID = 39671
dacht.net.	3600	IN	KEY	256  3  5 ( 
			AQPDvaQlOBmS/l1T415xdfnIpgpm06aKslQq
			9oMJnLbDzB3H8ByJbKWdTMrztzdru+mO2ZT+
			y13/cRh6tRzoO2eqZzhZECzTP4FoSGiJuYVh
			EabYi/hiNLzUYQ1Rgj2Tf3k= 
			) ; Key ID = 44699
dacht.net.	3600	IN	KEY	256  3  5 ( 
			AQMDMZAo5Or5ITcpcBm2AqDzi43eKRAZrHtU
			aBfeuZ4qU+Br9US7veW0uOcfDKcGoO4de+YL
			1f3JVNeH3X/6fghFlCIKsH3J+XRuVgv6HlP/
			Cmbnz+GUOy9IiutUb0a0kPFB2td9 
			) ; Key ID = 21827
";
is( $key->get_keyset("dacht.net"), $keyset2, "getkeyset returns published keys as well");
     			                                        #test 56




is ($key->set_inactive,1,"Non trivial toggling worked again");
     			                                        #test 57

ok ( $key->is_inactive,"Key is inactive");
     			                                        #test 58

ok (! $key->is_active,"Key is inactive, thus not active");
     			                                        #test 59

ok (! $key->is_published,"Key is inactive, thus not published");
     			                                        #test 60

ok ( $key->is_algorithm("RSASHA1"),"Key is proper algorithm");  # test 61
ok ( $key->is_algorithm(5),"Key is proper algorithm");	        # test 62
ok ( ! $key->is_algorithm("RSA"),"is_algorithm (\"RSA\")");     # test 63
ok ( ! $key->is_algorithm("DSA"),"is_algorithm (\"DSA\")");     # test 64
ok ( ! $key->is_algorithm("CRYPTSAM"),"is_algorithm (\"CRYPTSAM\")"); # test 65




SKIP: {
  skip $skip_creation?"Skipping creation tests":"No dnssec-keygen found on the system", 6 if $nokeygen || $skip_creation;
  my $key3=Net::DNS::SEC::Maint::Key->new();
  #  Create a key 
  $key3->create("bla.foo","RSASHA1",768,"zonesigning");
  $key3->create("bla.foo","RSASHA1",768,"zonesigning");
  my @keyarray2;
  is (@keyarray2=$key3->get_all("bla.foo"),2,"two keys for bla.foo returned");
     			                                        #test 66
  my @activekeys;
  is ( @activekeys=$key3->get_active("bla.foo"),1,"One activekey for bla.foo");
     			                                        #test 67

  $activekeys[0]->set_rollover;
  ok ( $activekeys[0]->is_rollover,"Key has rollover attribute set");

     			                                        #test 68

  $activekeys[0]->set_inactive;
  ok ( $activekeys[0]->is_inactive,"Key is inactive");
     			                                        #test 69
  $activekeys[0]->set_active;
  ok ( $activekeys[0]->is_active,"Key is active");
     			                                        #test 70
}









# Clean the mess.
#
unlink <t/keydb/bla.foo/Expired_Keys/*>;
rmdir "t/keydb/bla.foo/Expired_Keys/";
unlink <t/keydb/bla.foo/*>;
rmdir "t/keydb/bla.foo";

diag ("tests are finished");
