# -*-perl-*-
# Test script for  Net::DNS::SEC::Maint::Key::Rollover
# $Id: 02-rollover.t,v 1.7 2005/06/24 08:39:30 olaf Exp $
# 


use Test::More;
use Data::Dumper;
use strict;

########################################################
# Log 4 perl initialization
use Log::Log4perl;
my $log4perlconf="t/log4perl.conf";

Log::Log4perl::Config->allow_code(0);
if (-f $log4perlconf ){
    Log::Log4perl->init($log4perlconf)
    }
else {
    my $conf = q(
 log4perl.rootLogger                = DEBUG, LOGFILE
 log4perl.appender.LOGFILE          = Log::Log4perl::Appender::File
 log4perl.appender.LOGFILE.filename = test.log
 log4perl.appender.LOGFILE.layout   = Log::Log4perl::Layout::PatternLayout
 log4perl.appender.LOGFILE.layout.ConversionPattern = %d %p> %F{1}:%L %M - %m%n
 );
    Log::Log4perl->init(\$conf);
}

####################################



my $skip_creation=0; # skips a number of tests .. set to 0 in distribution


# Clean up berfore starting.... If there are any keys left from a previous run
# we will have failures
unlink <t/keydb/bla.foo/*>;
rmdir "t/keydb/bla.foo";



diag ("The tests depend on dnssec-keygen and the openssl command to be in your path");


my $nokeygen=0;
my $noopenssl=0;

my $dnssec_keygen_path;
my $openssl_path;
use Shell qw (which);
$dnssec_keygen_path = which("dnssec-keygen");
$dnssec_keygen_path =~ s/\s+$//;

$openssl_path = which("openssl");
$openssl_path =~ s/\s+$//;

if (!$dnssec_keygen_path ){
    diag "dnssec-keygen not found\n";
    diag "Change your path to include dnssec_keygen\n";
    $nokeygen=1;
}


if (!$openssl_path ){
    diag "openssl not found\n";
    diag "Change your path to include openssl\n";
    $nokeygen=1;
}

# I wonder how portable this is...
if ($dnssec_keygen_path =~ /no *. in/ ){
  diag $dnssec_keygen_path ."\n";
  diag "Change your path to include dnssec_keygen\n";
  $nokeygen=1;
}
#extra test
if ( !( -x $dnssec_keygen_path )){
  diag $dnssec_keygen_path." is not executable\n";
  diag "Change your path to include dnssec_keygen\n";
  $nokeygen=1;
}


# I wonder how portable this is...
if ($openssl_path =~ /no *. in/ ){
  diag $openssl_path ."\n";
  diag "Change your path to include openssl\n";
  $nokeygen=1;
}
#extra test
if ( !( -x $openssl_path )){
  diag $openssl_path." is not executable\n";
  diag "Change your path to include openssl\n";
  $nokeygen=1;
}

$ENV{"DNSSECMAINT_DNSSEC_KEYGEN"}=$openssl_path. " rand -out t/random 100000 ;".$dnssec_keygen_path. " -r t/random ";



if ($nokeygen){
    plan skip_all =>  "critical programs not found";
    exit;
}else{
    plan tests=>20;
}



BEGIN {use_ok('Net::DNS::SEC::Maint::Key');                                  # test 1
   };







BEGIN {
  use_ok('Net::DNS::SEC::Maint::Key');                                  # test 2
     };




my $confpath="t/dnssecmaint.conf";
$ENV{"DNSSECMAINT_CONFFILE"}=$confpath;

$ENV{"PATH"}=$ENV{"PATH"}.":/usr/local/sbin/:";

my $gid=getgrnam "dnssecmt";
if (!defined $gid){
  print "    dnssecmt is an unknown group\n";
  print "    Please add the group to /etc/group or change the maintgroup \n";
  print "    I will try to use your default group.\n";
  $ENV{"DNSSECMAINT_MAINTGROUP"}=getgrgid($();
}





my $key=Net::DNS::SEC::Maint::Key->new;
ok($key,                "new() returned something");             # test 3

is (ref($key),"Net::DNS::SEC::Maint::Key","Net::DNS::SEC::Key object creation");
                                                                 # test 4


is ( $key->rollover_zone("dacht.net","SLA1"),"SLA1 is not a valid algorithm","rollover_zone refuses bogus algorithm");
                                                                 # test 5


is ($key->rollover_zone("dacht.net","RSASHA1"),"automatic rollover of zone signing keys is only possible if there is one published zone signing key","Number of published keys is tested");
                                                                 # test 6


#
#  This is about all tests we can do without creating new entries in the database. We will need to have 
#  the dnssec keygen command available and such...
# 


diag ("The tests depend on dnssec-keygen and the openssl command to be in your path");




$ENV{"DNSSECMAINT_DNSSEC_KEYGEN"}=$openssl_path. " rand -out t/random 100000 ;".$dnssec_keygen_path. " -r t/random ";




is ($key->rollover_zone("bla.foo","RSASHA1"),"No such domain: bla.foo","Failure if domain does not exist");
                                                                # test 7

SKIP: {
  skip $skip_creation?"Skipping creation tests":"No dnssec-keygen found on the system", 2 if $nokeygen || $skip_creation;

  #  Create a key 
  $key->create("bla.foo","RSASHA1",768,"zonesigning",1);
  ok ( ($key->get_active("bla.foo"))[0]->get_keypath =~ /^t\/keydb\/bla\.foo\/Kbla\.foo\.\+005\+(\d+)\.private$/, 
       "active zone keys for test zone");
                                                                # test 8

  is ($key->rollover_zone("bla.foo","RSASHA1"),"automatic rollover of zone signing keys is only possible if there is one published zone signing key","Number of active keys is tested");
                                                                 # test 9

  $key=$key->create("bla.foo","RSASHA1",768,"zonesigning",0);
  $key->set_published;
  diag ("If the following test fails we will have a cascade of failing tests");
  is (ref($key),"Net::DNS::SEC::Maint::Key","Net::DNS::SEC::Key object returned from create method");
  my @rollover_keys=$key->get_rollover("bla.foo");

  ok (@rollover_keys==0,"get_rollover returned 0 keys");
                                                                  # test 11


  is ($key->rollover_zone("bla.foo","RSASHA1"),0,"rollover succes");
                                                                  # test 12


  ok($key->is_active, "rollover made published key active");
                                                                  # test 13
  @rollover_keys=();
  @rollover_keys=$key->get_rollover("bla.foo");
  ok (@rollover_keys==1,"get_rollover returned 1 key");
                                                                  # test 14
  ok ($rollover_keys[0]->is_rollover,"get_rollover returned a key with rollover attribute set");
                                                                  # test 15

  is ($key->rollover_zone("bla.foo","RSASHA1"),0,"rollover succes");
                                                                  # test 16

  diag "Key creation, may take a while, be patient";
  $key=$key->create("bla.foo","RSASHA1",768,"keysigning",1);
  my $createdkeyid=$key->get_keyid;

  is (ref($key),"Net::DNS::SEC::Maint::Key","Net::DNS::SEC::Key object returned from create method");
                                                                  # test 17  
  is ($key->rollover_key("bla.foo","RSASHA1"),0,"rollover succes");
                                                                  # test 18

  is ( ($key->get_active_key("bla.foo")),2,"two active keys during the rollover");
                                                                  # test 19
  my @rolloverkey=$key->get_rollover("bla.foo");
  is( $rolloverkey[0]->get_keyid, $createdkeyid, "Get rollover returned the proper key.");
                                                                  # test 20
  is ($key->rollover_key("bla.foo","RSASHA1"),0,"rollover succes");
                                                                  # test 21
  is ( ($key->get_active_key("bla.foo")),1,"one active key after the rollover");
                                                                  # test 22

  

 };  #  end SKIP







# Clean the mess.
#
unlink <t/keydb/bla.foo/Expired_Keys/*>;
rmdir "t/keydb/bla.foo/Expired_Keys/";
unlink <t/keydb/bla.foo/*>;
rmdir "t/keydb/bla.foo/";

diag ("tests are finished");

