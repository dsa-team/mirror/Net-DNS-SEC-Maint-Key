=head1 NAME

Net::DNS::SEC::Maint::Maint::Key::Config - Configuration class for KEY related work



=head1 DESCRIPTION

This module is used to get to configuration paramaters. 

=cut


package Net::DNS::SEC::Maint::Key::Config;

use Net::DNS::SEC::Maint::Config;
use strict;
use IO::File;
use Log::Log4perl qw(get_logger :levels);

use vars qw($VERSION 
	    @EXPORT 
	    @ISA
	    $AUTOLOAD 
	    );


@ISA= qw( Net::DNS::SEC::Maint::Config);
$VERSION = do { my @r=(q$Revision: 1.3 $=~/\d+/g); sprintf "%d."."%03d"x$#r,@r };


=head1 Paramaeter and default values

  Names and defaults of the parameters. The names of the ENVIRONMENT variables that
  overwrite the default values of the values specified in the confuration file are
  in capital letters and between brackets.

=cut


sub set_default_conf {
  my $self=shift;  
  print "Net::DNS::Key::Config set_default_conf called \n" if
    $Net::DNS::SEC::Maint::Config::debug;


  my $home_dir=$ENV{"HOME"};
  #
  # First load all the Generic shell defaults
  $self->Net::DNS::SEC::Maint::Config::set_default_conf;

  $self->setconf('conffile',$Net::DNS::SEC::Maint::Config::conffile,
		 "specifies where the configuration file can be found");


  # Now load the configuration defaults specific for the KEY shell.


  # ssh_keygen_command
  # Program used to convert SSHC type key to keyformat used in authorized
  # keys file. Including the flags. Note we only tested this for openssh.


=head2 dnssec_keygen_command (DNSSECMAINT_DNSSEC_KEYGEN)    

    full path to the dnssec-keygen command, including flags. Defaults to:
    /usr/local/sbin/dnssec-keygen -r /dev/urandom

=cut

    $self->setconf('dnssec_keygen',"/usr/local/sbin/dnssec-keygen -r /tmp/random",
		   "full path to BIND's dnssec-keygen command with optional arguments"
		   );




=head2 dnssec_signzone_command (DNSSECMAINT_DNSSEC_SIGNZONE)    

    full path to the dnssec-signzone command, including flags. Defaults to:
    /usr/local/sbin/dnssec-signzone -r /dev/urandom

=cut

    $self->setconf('dnssec_signzone',"/usr/local/sbin/dnssec-signzone -r /dev/urandom",
		   "full path to BIND's dnssec-signzone command with optional arguments"
		   );


=head2 dns_key_db (DNSSECMAINT_DNS_KEY_DB)    

    directory under which the dnssec key database is stored.  defaults
    to the /usr/local/var/dnssec_maint/dnDNS_Key_DB 

=cut

    $self->setconf('dns_key_db',"/usr/local/var/dnssec_maint/DNS_Key_DB",
		   "Path to the directory in which the key database is kept"
		   );



=head2 tmpdir (DNSSECMAINT_TIMPDIR)    


    to the /tmp/

=cut

    $self->setconf('tmpdir',"/tmp/",
		   "Path to the directory in which temporary files are stored"
		   );

=head2 dsakeysizekey (DNSSECMAINT_DSAKEYSIZEKEY)

    Default size for DSA keysigning keys (1024)

=head2 rsakeysizekey (DNSSECMAINT_RSAKEYSIZEKEY)

    Default size for RSA keysigning keys (2048)

=head2 dsakeysizezone (DNSSECMAINT_DSAKEYSIZEZONE)


    Default size for RSA keysigning keys (512)

=head2 rsakeysizezone (DNSSECMAINT_RSAKEYSIZEZONE)

    Default size for RSA zonesigning keys (748)

=cut

    $self->setconf('DSAKEYSIZEKEY', 1024,
		   "Default size for DSA Key Signing Keys"
		   );
    $self->setconf('DSAKEYSIZEZONE', 512,
		   "Default size for DSA Zone Signing Keys"
		   );

    $self->setconf('RSAKEYSIZEKEY', 2048,
		   "Default size for RSA Key Signing Keys"
		   );
    $self->setconf('RSAKEYSIZEZONE', 768,
		   "Default size for RSA Zone Signing Keys"
		   );

    1;

}









1;
